const toggle = document.getElementById("togBtn");
const price1 = document.getElementById("price1");
const price2 = document.getElementById("price2");
const price3 = document.getElementById("price3");

window.onload = () => {
    // fixes a bug where tick in the checkmark is retained after the page reload
    toggle.checked = false;
}

toggle.onclick = () => {
    if (toggle.checked) {
        console.log("True");
        price1.innerHTML = "&dollar;199.99";
        price2.innerHTML = "&dollar;249.99";
        price3.innerHTML = "&dollar;399.99";

    } else {
        console.log("False");
        price1.innerHTML = "&dollar;19.99";
        price2.innerHTML = "&dollar;24.99";
        price3.innerHTML = "&dollar;39.99";
    }
}
